#!/bin/bash

success=0
error=0

for i in $*; do
if ping -c 1 $i &> /dev/null;then
	success=$((success +1))
else
	error=$((error +1))
fi
done

echo "Sucesso: $success"
echo "erro: $error"
