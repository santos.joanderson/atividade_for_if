#!/bin/bash

arq=$1

if [ -e $arq ]; then
 echo "Ok"

else
 echo "Erro"
 exit 1
fi

echo "Digite 1 para as primeiras linhas."
echo "Digte 2 para as ultimas linhas."
echo "Digite 3 para exibir o número d linhas do arquivo"
read op

if [ $op = "1" ]; then
head -10 $arq
elif [ $op = "2" ]; then
tail -10 $arq
elif [ $op = "3" ]; then
wc -l $arq
else
echo "Erro"
fi
