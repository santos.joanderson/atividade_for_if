#!/bin/bash

a="uol.com.br"
b="globo.com"
c="ifpb.edu.br"
d="bug.com"

total=0


if ping -c 1 $a &> /dev/null; then
total=$((total+1))
echo "$a"
fi

if ping -c 1 $b &> /dev/null; then
total=$((total+1))
echo "$b"
fi

if ping -c 1 $c &> /dev/null; then
total=$((total+1))
echo "$c"
fi

if ping -c 1 $d &> /dev/null; then
total=$((total +1))
echo "$d"
fi

echo "$total"
